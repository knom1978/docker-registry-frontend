# docker-registry-toturial

A summary for using docker registry and related information.

#### Docker registry
https://hub.docker.com/_/registry/

At first, you have to edit daemon.json in /etc/docker
https://www.itread01.com/content/1504154408.html
```
{ "insecure-registries":["10.11.108.229:5000"] }
```
Then reload the daemon/restart docker
```
systemctl daemon-reload
systemctl restart docker
sudo docker run -d -p 5000:5000 -v /opt/registry/:/tmp/registry registry:2.7.1
```
The method to push
```
docker pull python:3.6.8-jessie
sudo docker tag python:3.6.8-jessie 10.11.108.229:5000/python:3.6.8-jessie
sudo docker push 10.11.108.229:5000/python:3.6.8-jessie
curl http://10.11.108.229:5000/v2/_catalog
```



The method to backup

https://docs.docker.com/ee/dtr/admin/disaster-recovery/create-a-backup/#backup-dtr-metadata

#### Docker registry ui
https://github.com/Joxit/docker-registry-ui
```
sudo docker run -d -p 80:80 -e REGISTRY_URL=http://10.11.108.229:5000 -e DELETE_IMAGES=true -e REGISTRY_TITLE="SMG registry" joxit/docker-registry-ui:static
```

#### Docker registry frontend (reference)
https://hub.docker.com/r/konradkleine/docker-registry-frontend/
```
docker run -d -p 5000:5000 --restart always --name registry registry:2
```
The method to use
```
sudo docker run \
  -d \
  -e ENV_DOCKER_REGISTRY_HOST=ENTER-YOUR-REGISTRY-HOST-HERE \
  -e ENV_DOCKER_REGISTRY_PORT=ENTER-PORT-TO-YOUR-REGISTRY-HOST-HERE \
  -p 8080:80 \
  konradkleine/docker-registry-frontend:v2
```
#### Harbor (reference)
https://www.jianshu.com/p/fc544e27b507
https://github.com/goharbor/harbor/blob/master/docs/user_guide.md